# Building OpenRGB in WSL

## Getting Set Up

You will need a few things to get started

1. An X server (I am using [GWSL](https://opticos.github.io/gwsl/) )

2. A WSL linux distro (I am using ubuntu 20.04 LTS)

3. And Qt

## Setting up linux

### Installing Qt

Start by running ``sudo apt install git build-essential qtcreator qt5-default libusb-1.0-0-dev libhidapi-dev pkgconf`` (Assuming you are using ubuntu)

After that command has completed you will need to run ``sudo strip --remove-section=.note.ABI-tag /usr/lib64/libQt5Core.so.5`` otherwise you will get ``libQt5Core.so.5 missing`` errors

### Building OpenRGB

To get Qt creator to appear you will need to start your X server (I will give examples using GWSL because it is free and painless)

Make sure you have the X server set to auto export or you will have to do it manually (Which idk how to do 😛)

![The main window](OpenRGB_On_WSL/Main.png)

![Auto Export](OpenRGB_On_WSL/Auto_Export.png)

Now you can use git clone to download OpenRGB (``git clone https://gitlab.com/CalcProgrammer1/OpenRGB.git`` )

With that done you can now run ``qtcreator`` (from the terminal) or start it from WSL under the ``Linux Apps`` section (Qt **Creator**, **NOT** Qt Designer, linguist, or any other software they may have)
