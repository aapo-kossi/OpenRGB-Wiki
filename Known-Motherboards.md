# Known Asus Motherboards

## ASUS PRIME X370-PRO

Chipset: AMD X370

I2C Driver: i2c_smbus_piix4, at 0x0B00 and 0x0B20

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: LED-0116 [Firmware](uploads/39b9aada66f0976d9558cd0db03fab77/primex370pro.bin)

## ASUS ROG CROSSHAIR VI HERO

Chipset: AMD X370

I2C Driver: i2c_smbus_piix4, at 0x0B00 and 0x0B20

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: LED-0116 [Firmware](uploads/8e01471313adaaeafe27de360eb8102f/crosshairvihero.bin)

## ASUS PRIME X470-PRO

Chipset: AMD X470

I2C Driver: i2c_smbus_piix4, at 0x0B00 and 0x0B20

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: AUMA0-E6K5-0106

## ASUS ROG STRIX X470-F

Chipset AMD X470

I2C Driver: i2c_smbus_piix4, at 0x0B00 and 0x0B20

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: AUMA0-E6K5-0106

## ASUS ROG STRIX B450-I

Chipset: AMD B450

I2C Driver: i2c_smbus_piix4, at 0x0B00 and 0x0B20

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: AUMA0-E6K5-0105

## ASUS ROG RAMPAGE VI Extreme

LED EC Version: AUMA0-E6K5-0105

LED EC Version 2: AULA-S072-0201

## ASUS PRIME Z270-A

Chipset:  Intel Z270, Nuvoton NCT6793D

I2C Driver: i2c_smbus_i810, i2c_smbus_nuvoton_nct6793d (at 0x2C0)

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: LED-0116

## ASUS ROG Strix Z370-E

Chipset:  Intel Z370, Nuvoton NCT6798D

I2C Addresses: 0x4E (motherboard Aura)

LED EC Version: AUMA0-E6K5-0106

LED EC Version 2: AULA2-S072-0101
