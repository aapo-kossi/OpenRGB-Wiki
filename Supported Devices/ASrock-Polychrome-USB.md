# ASrock Polychrome USB

 ASRock Polychrome controllers will save with each update.
Per ARGB LED support is not possible with these devices.

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `26CE` | `01A2` | ASRock Polychrome USB |
| `26CE` | `01A6` | ASRock Deskmini Addressable LED Strip |
