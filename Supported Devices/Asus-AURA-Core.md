# Asus AURA Core

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is problematic (See device page for details)

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `1854` | ASUS Aura Core |
| `0B05` | `1866` | ASUS Aura Core |
| `0B05` | `1869` | ASUS Aura Core |
