# Thermaltake Riing Quad

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `264A` | `2260` | Thermaltake Riing Quad (PID 0x2260) |
| `264A` | `2261` | Thermaltake Riing Quad (PID 0x2261) |
| `264A` | `2262` | Thermaltake Riing Quad (PID 0x2262) |
| `264A` | `2263` | Thermaltake Riing Quad (PID 0x2263) |
| `264A` | `2264` | Thermaltake Riing Quad (PID 0x2264) |
| `264A` | `2265` | Thermaltake Riing Quad (PID 0x2265) |
| `264A` | `2266` | Thermaltake Riing Quad (PID 0x2266) |
| `264A` | `2267` | Thermaltake Riing Quad (PID 0x2267) |
| `264A` | `2268` | Thermaltake Riing Quad (PID 0x2268) |
| `264A` | `2269` | Thermaltake Riing Quad (PID 0x2269) |
| `264A` | `226A` | Thermaltake Riing Quad (PID 0x226A) |
| `264A` | `226B` | Thermaltake Riing Quad (PID 0x226B) |
| `264A` | `226C` | Thermaltake Riing Quad (PID 0x226C) |
| `264A` | `226D` | Thermaltake Riing Quad (PID 0x226D) |
| `264A` | `226E` | Thermaltake Riing Quad (PID 0x226E) |
| `264A` | `226F` | Thermaltake Riing Quad (PID 0x226F) |
| `264A` | `2270` | Thermaltake Riing Quad (PID 0x2270) |
