# GaiZhongGai Keyboard

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `3061` | `4700` | GaiZhongGai 68+4 PRO |
| `3061` | `4770` | GaiZhongGai 17+4+Touch PRO |
| `3061` | `4771` | GaiZhongGai 17 PRO |
| `3061` | `4772` | GaiZhongGai 20 PRO |
