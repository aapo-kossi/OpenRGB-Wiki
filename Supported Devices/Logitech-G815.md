# Logitech G815

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C232` | Logitech G813 RGB Mechanical Gaming Keyboard |
| `046D` | `C33F` | Logitech G815 RGB Mechanical Gaming Keyboard |
