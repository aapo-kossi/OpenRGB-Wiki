# Trust GXT 180

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `145F` | `0248` | Trust GXT 180 |
