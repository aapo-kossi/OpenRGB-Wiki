# Understand and use Visual maps

## What are visual maps?

Visual maps allow you to apply effects to all your desired hardware, and place them on a grid.

[<img src="vm_example.png" height="384">](vm_exemple.png)

When everything is configured correctly, your effect will apply to all your included devices, depending on their placement on the grid.

## Create your first visual map

To create a visual map click **"New Map"** on the top left corner.

You should now have an empty grid. From there, you can add your devices from the left list, and move them as desired.

Then you need to check **"Register controller"**, and your visual map should now appear under your devices tab.

[<img src="devices_tab.png" height="384">](devices_tab.png)

You can now save your Map.

On the right you have options:

- You can adjust your visual map height and width
- Auto load will load your map at startup
- Auto register will register your map at load

**Be sure to save your map for these changes to take effect at startup.**

## Going further

Thanks to visual maps, you can also edit your devices.

Select a device in the left list or on the grid, and new options appears at the bottom right.

With them you can change your device orientation, or even its shape by selecting **"Custom"**.

[<img src="device_resize.png" height="384">](device_resize.png)

From the custom shape menu, you can select one or multiple LEDs and **Identify** them. All LEDs will turn off, except those you have selected.

[<img src="identify.png" height="256">](identify.png)

### Some examples

Here are some custom shapes examples:

**<ins>Circle</ins>**

[<img src="circle.png" height="256">](circle.png)

**<ins>Square</ins>**

[<img src="square.png" height="256">](square.png)

**<ins>Zigzag</ins>**

[<img src="zigzag.png" height="256">](zigzag.png)

By default, when nothing is selected, shape will apply to all LEDs.
You can apply it to only desired LEDs by selecting them.

## Background

You can also apply a **background** to your visual map.

To do so, tick the **background** icon.

You can place your background on the map by using **X and Y offsets**.

Then add one or multiples colors, and adjust them to create your own background.

[<img src="custom_background.png" height="256">](custom_background.png)

Otherwise, you can import your own image and use it as background.

OpenRGB will adapt your image to your visual map size, and import it.

[<img src="bg_example.png" height="128">](bg_example.png)[<img src="bg_set.png" height="128">](bg_set.png)

**<ins>Feel free to explore the possibilities to create your perfect lighting environment.</ins>**

![complete_map.png](complete_map.png)
